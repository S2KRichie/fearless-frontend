window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
        // grabbing data
        const data = await response.json();
        // selecting the select tag and assigning to the variable element
        const element = document.getElementById('state');
        console.log(element)
        // iterate through data.states; state object = {name, abbreviation}
        for (let state of data.states) {
            // creating option element tag assigning to optionelement
            const optionElement = document.createElement('option');
            //
            optionElement.value = state.abbreviation;
            optionElement.label = state.name;
            element.appendChild(optionElement);
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }
        });
    }
});
