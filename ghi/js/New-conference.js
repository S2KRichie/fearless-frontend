window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        console.log(data.locations)
        const element = document.getElementById('location');
        console.log(element)
        for (let location of data.locations) {
            const optionElement = document.createElement('option');
            optionElement.value = location.id;
            optionElement.label = location.name;
            element.appendChild(optionElement);
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
            }


        })

    }
});
