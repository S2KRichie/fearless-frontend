function createCard(name, description, pictureUrl, endTime, startTime, locationName) {
  return `
  <div class="col-8 col-sm-6">
    <div class="shadow-lg p-3 mb-5 bg-white rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
          <h5 class="card-title">${name}</h5>
            <div class="card-subtitle mb-2 text-muted">
              ${locationName}
            </div>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          <p>${startTime} - ${endTime}</p>
        </div>
      </div>
    </div>
  </div>
  `;
}


  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startTime = new Date(details.conference.starts).toLocaleDateString();
            const endTime = new Date(details.conference.ends).toLocaleDateString();
            const locationName = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, endTime, startTime, locationName);
            const row = document.querySelector('.row');
            row.innerHTML += html;

          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
